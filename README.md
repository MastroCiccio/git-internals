# LABORATORIO 8 NOVEMBRE 2018

## Obiettivi

- Esplorare gli internals di Git
- Familiarizzare con Git, hosting remoto

## Installazioni


    
- (se necessario) registrarsi sul sito https://gitlab.com  
  _è possibile usare anche una delle seguenti login_:
  ![image](/uploads/6620f21374789a961b182afc237596b4/image.png)


## Lavoro

1. fare `fork` del repository https://gitlab.com/svigruppo/git-internals
2. fare clone in locale  
   Troverete un ramo `develop` e un ramo `master` oltre ad alcuni commmit taggati con vari nomi
3. se volete usare Eclipse date il comando `./gradlew eclipse` e poi potete importare il progetto da Eclipse come _existing project_
4. se volete usare IntelliJ date il comando `./gradlew idea` e poi potete importare il progetto da IntelliJ

Procedere iterativamente a questo punto (lavorando sul ramo `develop`)
- guardando il test (scritto in JUnit) presente nella directory `src/test/java/core`  
  _potete eseguirlo sia dalla IDE che dalla SHELL con il comando_ `./gradlew test`
- scrivendo il codice necessario per farlo passare (le classi vanno messe in `src/main/java/core`)
- ripulendo il codice (refactoring)
- incorporando il successivo test (vedi sotto)

Ogni volta che superate un nuovo test e siete soddisfatti della vostra soluzione, pubblicate tale soluzione sul ramo `master`.
Al termine, ovunque siate arrivati, prevedete di "consegnare" il compito taggando la versione sul `master` come `ultimoRilascio` e e taggando il commit più recente (probabilmente quello puntato da develop) come `fineLaboratorio`. Quindi eseguire il push di entrambi i rami (`git push --all --tags`).

Chiaramente siete invitati nel caso non abbiate completato il lavoro a farlo a casa e a fine lavoro taggarlo come "fineLavoro".



### NOTE

- Per incorporare il test successivo da implementare, usare il comando `git cherry-pick <tagname>`
- Dove nei test trovate `???` dovete seguire le seguire le indicazioni dei commenti-TODO e sostuirli con i valori trovati tramite operazioni nella shell

### SUGGERIMENTI
- per la funzione di inflate si può comodamente usare la classe `InflaterInputStream` o la funzione `inflate` dalla libreria standard `java.util.zip`
- per documentazione riguardo ai formati internals di Git potete riferirvi a:
    - https://git-scm.com/book/en/v2/Git-Internals-Git-Objects
    - http://stackoverflow.com/a/21599232
- fate attenzione per le parti binarie del file (ad es. oggetto tree) a come trattate i dati (se come byte o stringhe con uno specifico encoding)

