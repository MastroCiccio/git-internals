package core;

import java.io.FileInputStream;
import java.util.Arrays;
import java.util.zip.InflaterInputStream;

class GitObject {

    String repoPath;
    private String hash;

    GitObject(String repoPath, String hash) {
        this.repoPath = repoPath;
        this.hash = hash;
    }

    String getHash() {
        return this.hash;
    }

    byte[] getDecompressedDataAsByteArray() {
        String hashPath = this.repoPath + "/objects/" + this.hash.substring(0, 2) + "/" + this.hash.substring(2);
        int size = 1000;
        byte[] buffer = new byte[size];
        try {
            InflaterInputStream inf = new InflaterInputStream(new FileInputStream(hashPath));
            int read = inf.read(buffer, 0, size);
            buffer = Arrays.copyOfRange(buffer, 0, read);
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }
        return buffer;
    }

    String getDecompressedData() {
        String hashPath = this.repoPath + "/objects/" + this.hash.substring(0, 2) + "/" + this.hash.substring(2);
        String data = "";
        try {
            int size = 1000;
            byte[] buffer = new byte[size];
            InflaterInputStream inf = new InflaterInputStream(new FileInputStream(hashPath));
            int read = inf.read(buffer, 0, size);
            data = new String(buffer, 0, read);
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }
        return data;
    }

    String getType() {
        return getDecompressedData().split(" ")[0];
    }

    String getContent() {
        return getDecompressedData().split("\0", 2)[1];
    }
}
