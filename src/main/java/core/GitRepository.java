package core;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class GitRepository {

    private String repoPath;

    GitRepository(String repoPath) {
        this.repoPath = repoPath;
    }

    public String getHeadRef() {
        BufferedReader headFile;
        String ref = "";
        try {
            headFile = new BufferedReader(new FileReader(this.repoPath + "/HEAD"));
            ref = headFile.readLine().substring(5);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return ref;
    }

    public String getRefHash(String ref) {
        try {
            return new BufferedReader(new FileReader(this.repoPath + "/" + ref)).readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "not found";
    }
}
