package core;

import java.util.HashMap;

class GitTreeObject extends GitObject {

    private HashMap<String, GitObject> entries;

    GitTreeObject(String repoPath, String masterTreeHash) {
        super(repoPath, masterTreeHash);
        this.entries = new HashMap<>();
    }

    String[] getEntryPaths() {
        byte[] buf = getDecompressedDataAsByteArray(),
                entryBuf = new byte[100],
                hashBuf = new byte[20];

        String[] entry;
        String hash;
        int i = 0, j;

        while((char) buf[i] != '\0') i++; //skip type and size

        while(++i < buf.length) {
            for(j = 0; buf[i] != '\0'; j++) {
                entryBuf[j] = buf[i++];
            }
            entry = new String(entryBuf, 0, j).split(" ");

            for(j = 0; j < 20; j++) {
                hashBuf[j] = buf[++i];
            }
            hash = getHashFromHexBytes(hashBuf);

            this.entries.put(entry[1], pickObject(entry[0], hash));
        }

        return this.entries.keySet().toArray(new String[0]);
    }

    private String getHashFromHexBytes(byte[] raw) {
        char[] alphabet = "0123456789abcdef".toCharArray();
        char[] result = new char[raw.length * 2];

        for(int i=0, index; i < raw.length; i++)
        {
            index = raw[i] & 0xFF;
            result[2*i] = alphabet[index >> 4];
            result[2*i + 1] = alphabet[index & 0x0F];
        }

        return new String(result);
    }

    private GitObject pickObject(String mode, String hash) {
        if(mode.startsWith("1"))
            return new GitBlobObject(this.repoPath, hash);

        return new GitTreeObject(this.repoPath, hash);
    }

    GitObject getEntry(String entry) {
        return this.entries.get(entry);
    }
}
