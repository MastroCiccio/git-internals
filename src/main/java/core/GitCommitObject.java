package core;

public class GitCommitObject extends GitObject {

    GitCommitObject(String repoPath, String hash) {
        super(repoPath, hash);
    }

    public String getTreeHash() {
        return getDecompressedData().split("tree ")[1].substring(0, 40);
    }

    public String getParentHash() {
        return getDecompressedData().split("parent ")[1].substring(0, 40);
    }

    public String getAuthor() {
        String data = getDecompressedData().split("author ")[1];
        int index = data.indexOf(">");
        return data.substring(0, index+1);
    }
}
